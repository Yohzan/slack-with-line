// メイン関数

//’e’はPOSTされてきたデータ
function doPost(e)
{
  console.info('Start:トリガーが発動しました。プログラムを開始します。');

  //POSTデータを使える形に変形
  var line = JSON.parse(e.postData.contents).events[0];

  console.info('取得したJSONデータを出力します。');
  console.log(line);

  switch (line.type)
  {
    //送られてきた情報がメッセージなら
    case 'message':
      console.info('LINEからメッセージを受け取りました。送信元の情報を解析します。');

      //送信者の情報を取得
      var profile = get_line_profile(line);

      //送信者情報を引数にSlackにメッセージを転送
      postSlackMessage(get_line_message(line.message), profile);

      console.info('Success:プログラムを終了します。');
      break;

    //送られてきた情報がLINEグループへの参加なら
    case 'join':
      console.info('BotがLINEグループに参加しました。グループIDをスプレッドシートに記録します。');

      //スクリプトのプロパティから使用するスプレッドシートの情報を取得
      var sheet_ID = PropertiすesService.getScriptProperties().getProperty('sheet_ID');
      var sheet_name = PropertiesService.getScriptProperties().getProperty('sheet_name');
      var spreadsheet = SpreadsheetApp.openById(sheet_ID);
      var sheet = spreadsheet.getSheetByName(sheet_name);

      //スプレッドシートのA1のマスにグループIDを記入
      sheet.getRange("A1").setValue(line.source.groupId);

      console.log('グループIDは"' + line.source,groupId + '"です。');
      console.info('Success:プログラムを終了します。');
      break;

    default:
      console.errror('Error:誤ったデータを受け取りました。POSTされたJSONデータを確認してください。プログラムを終了します。');
      break;
  }
}
