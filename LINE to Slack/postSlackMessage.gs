function postSlackMessage(mes, profile)
{
  //BOTの名前とアイコンを送信者のものに書き換える
  if (profile != undefined)
  {
    var name = profile.displayName;
    var icon = profile.pictureUrl;

    var options = {
      username: name,
      icon_url: icon
    };

    console.info('送信者情報を確認しました。');
  }
  //送信者の情報が不明な場合
  else
  {
    //送信者不明なときのLINEのアイコンのURL
    var icon_url = PropertiesService.getScriptProperties().getProperty('icon_url');

    var options = {
      username: 'LINE',
      icon_url: icon_url
    };

    console.warn('送信者情報が不明です。');
  }

  //Slack APIの導入
  var slack_token = PropertiesService.getScriptProperties().getProperty('slack_token');
  var slackApp = SlackApp.create(slack_token);

  //Slack　APIで　#line チャンネルにメッセージを送信
  slackApp.postMessage('line', mes, options);
  console.info('LINEからSlackへメッセージを転送しました。');
}