function get_line_message(message)
{
  console.info('受信データの解析へ移行します。');

  //送られたメッセージの種類を分類
  switch (message.type)
  {
    //文章の場合
    case 'text':
      console.info('受信データはテキストです。転送処理に移行します。');
      //そのままテキストを転送
      return message.text;

    //画像の場合
    case 'image':
      console.info('受信データは画像です。画像用転送処理に移行します。');
      //画像データを取得
      var blob = get_line_content(message.id);

      //画像データを転送
      postSlackFiles(blob.getAs('image/png').setName('line.png'));

      //画像はBotとして送信されるので、送信者を特定するためのテキストを転送
      return 'システムメッセージ:LINEから画像を送信しました。';

    //動画の場合
    case 'video':
      console.info('受信データは動画です。');
      //動画データを取得
      var blob = get_line_content(message.id);
      var file = DriveApp.createFile(blob);

      console.info('動画ファイルをDriveにアップロードしました。URLを転送します。');

      return file.getUrl();

    //音声ファイルの場合
    case 'audio':
      console.info('受信データは音声ファイルです。');
      var blob = get_line_content(message.id);
      var file = DriveApp.createFile(blob);

      console.info('音声ファイルをDriveにアップロードしました。URLを転送します。');

      return file.getUrl();

    //その他のファイルの場合
    case 'file':
      console.info('受信データはファイルデータです。');
      var blob = get_line_content(message.id);
      var file = DriveApp.createFile(blob);

      console.info('ファイルをDriveにアップロードしました。URLを転送します。');

      return file.getUrl();

    //位置情報の場合
    case 'location':
      var mapUrl = 'https://www.google.com/maps/search/?api=1&query={' + message.address + '}';

      console.info('受信データは位置情報です。GoogleマップのURLを出力します。');
      console.log(mapUrl);
      console.info('URLを転送します。');

      return mapUrl;

    //スタンプの場合
    case 'sticker':
      console.info('受信データはLINEスタンプです。LINEからURLを取得し送信します。');
      return 'https://stickershop.line-scdn.net/stickershop/v1/sticker/' + message.stickerId + '/android/sticker.png';

    default:
      console.warn('不明な受信データです。警告メッセージを転送します。');
      return '非対応のコンテンツです。LINEアプリから確認してください。';
  }
}
