function get_line_content(message_id)
{
  //スクリプトのプロパティからLINEBotのトークンを取得
  var line_token = PropertiesService.getScriptProperties().getProperty('line_token');

  //トークンをJSON化してAPIに渡せるようにする
  var headers = {'Authorization': 'Bearer ' + line_token};
  var options = {'headers': headers};

  //APIのURL
  var url = 'https://api.line.me/v2/bot/message/' + message_id + '/content';

  //APIにコンテンツファイルのURLを問い合わせ
  return UrlFetchApp.fetch(url, options);
}