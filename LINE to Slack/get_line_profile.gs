function get_line_profile(line)
{
  //スクリプトのプロパティから使用するスプレッドシートの情報の取得
  var sheet_ID = PropertiesService.getScriptProperties().getProperty('sheet_ID');
  var sheet_name = PropertiesService.getScriptProperties().getProperty('sheet_name');
  var spreadsheet = SpreadsheetApp.openById(sheet_ID);
  var sheet = spreadsheet.getSheetByName(sheet_name);

  //送信元のタイプで分類し、使用するLINEAPIを切り替え
  switch (line.source.type)
  {
    case 'user':
      console.info('このメッセージは個人チャットからの送信です。');
      var url = 'https://api.line.me/v2/bot/profile/' + line.source.userId;
      break;
    case 'group':
      console.info('このメッセージはLINEグループからの送信です。グループIDをスプレッドシートに上書きします。');
      var url = 'https://api.line.me/v2/bot/group/' + line.source.groupId + '/member/' + line.source.userId;
      sheet.getRange("A1").setValue(line.source.groupId);
      console.log('グループIDは"' + line.source.groupId + '"です。');
      break;
    case 'room':
      console.info('このメッセージは複数人チャットからの送信です。グループIDをスプレッドシートに上書きします。');
      var url = 'https://api.line.me/v2/bot/room/' + line.source.groupId + '/member/' + line.source.userId;
      sheet.getRange("A1").setValue(line.source.groupId);
      console.log('グループIDは"' + line.source,groupId + '"です。');
      break;
    default:
      console.warn('Warn:誤ったデータを受け取りました。送信者不明で続行します。');
      break;
  }

  //スクリプトのプロパティからLINEBotのトークンを取得
  var line_token = PropertiesService.getScriptProperties().getProperty('line_token');

  //トークンをJSON化してAPIに渡せるようにする
  var headers = {'Authorization': 'Bearer ' + line_token};
  var options = {'headers': headers};

  //APIにprofile問い合わせ
  var response = UrlFetchApp.fetch(url, options);

  //応答データを使える形に変形
  var profile = JSON.parse(response.getContentText());

  console.info('送信者の情報を出力します。');
  console.log(profile);

  return profile;
}