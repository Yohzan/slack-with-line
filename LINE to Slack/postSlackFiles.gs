function postSlackFiles(image)
{
  //スクリプトのプロパティからSlackBotのトークンを取得
  var bot_token = PropertiesService.getScriptProperties().getProperty('bot_token');

  //Slackにポストするデータ
  var data = {
    token:bot_token,
    file:image,
    channels:'line',
    title:'image',
  };
  var option = {
    'method':'POST',
    'payload':data
  };

  UrlFetchApp.fetch('https://slack.com/api/files.upload',option);
  console.info('画像をSlackに転送しました。送信者特定のためのテキストを転送します。');
}