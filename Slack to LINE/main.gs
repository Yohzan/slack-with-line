function doPost(e)
{
  console.info('START:トリガー発動によりプログラムを開始します。');
  var postData = JSON.parse(e.postData.getDataAsString());

  console.info('Slackからデータを受け取りました。出力します。');
  console.log(postData);

  switch(postData.type)
  {
    //SlackAPIに登録する際必要な応答
    case 'url_verification':
      console.info('このデータはVerificationです。Slackに対し適切なメッセージで応答します。');
      if(postData.type == 'url_verification') {
        var res = {'challenge':postData.challenge};
      }
      console.info('Success:応答完了後プログラムを終了します。');
      return ContentService.createTextOutput(JSON.stringify(res)).setMimeType(ContentService.MimeType.JSON);

    //転送するメッセージを受け取ったら
    case 'event_callback':
      console.info('Slackへの投稿を確認しました。転送処理に移行します。');

      var msg = getSlack_message(postData.event);

      if(msg != undefined)
      {
        postLINE_message(msg);
      }
      else
      {
        console.warn('Warning:空のメッセージを受け取りました。データを破棄しプログラムを終了します。');
      }
      break;

    default:
      console.error('Erroe:このデータは非対応です。プログラムを終了します。');
      break;
  }
}