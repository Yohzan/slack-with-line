function getSlack_filesUrl(data)
{
  //スクリプトのプロパティからSlackトークンの呼び出し
  var slack_token = PropertiesService.getScriptProperties().getProperty('slack_token');

  //スプレッドシートから投稿先グループIDを取得
  var sheet_ID = PropertiesService.getScriptProperties().getProperty('sheet_ID');
  var sheet_name = PropertiesService.getScriptProperties().getProperty('sheet_name');
  var spreadsheet = SpreadsheetApp.openById(sheet_ID);　　//スプレッドシートからグループIDを取得
  var sheet = spreadsheet.getSheetByName(sheet_name);
  var ID = sheet.getRange("A1").getValue();　

  console.log('送信先グループIDは"' + ID + '"です。転送処理へ移行します。');

  //postするデータ
  var post={
    token:slack_token,
    file:data.id
   };
  var slack_option={
    'method':'POST',
    'payload':post
  };
  //ファイルをパブリックに設定
  UrlFetchApp.fetch('https://slack.com/api/files.sharedPublicURL',slack_option);

  //Slackに直接ファイルが送信された場合
  if(event.files[0].permalink_public != undefined)
  {
    return data.permalink_public;
  }
  //クラウド経由でSlackにファイルが共有された場合
  else
  {
    return data.url_private;
  }
}
