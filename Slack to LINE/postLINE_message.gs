function postLINE_ｍessage(msg)
{
  //スクリプトのプロパティからLINEトークンを呼び出す
  var line_token = PropertiesService.getScriptProperties().getProperty('line_token');

  var sheet_ID = PropertiesService.getScriptProperties().getProperty('sheet_ID');
  var sheet_name = PropertiesService.getScriptProperties().getProperty('sheet_name');
  var spreadsheet = SpreadsheetApp.openById(sheet_ID);　　//スプレッドシートからグループIDを取得
  var sheet = spreadsheet.getSheetByName(sheet_name);
  var ID = sheet.getRange("A1").getValue();

  var postData = {
    "to": ID,
    "messages": [{
      "type": "text",
      "text": msg,
    }]
  };

  var headers = {
    "Content-Type": "application/json; charset=UTF-8",
    'Authorization': 'Bearer ' + line_token,
  };
  var options = {
    "method": "post",
    "headers": headers,
    "payload": JSON.stringify(postData)
  };
  return UrlFetchApp.fetch('https://api.line.me/v2/bot/message/push', options);
}