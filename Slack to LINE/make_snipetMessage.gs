function make_snipetMessage(data,userId)
{
  //SlackAppの導入
  var slack_token = PropertiesService.getScriptProperties().getProperty('slack_token');
  var slackApp = SlackApp.create(slack_token);

  //ユーザIDからユーザ情報獲得
  var userInfo = slackApp.usersInfo(userId);

  console.info('ユーザ名取得のため、ユーザ情報を獲得しました。出力します。');
  console.log(userInfo);

  var Name = userInfo.user.profile.display_name;

  //スニペットに付属テキストメッセージが存在する場合
  if(data.text!="")
  {
    console.info('このスニペットには付属テキストが存在します');
    //付属テキストに絵文字が含まれていれば変換
    if(data.text.match(/:/))
    {
      console.info('付属テキストには絵文字が含まれています。LINE用に変換します。');
      var text = emoji(data.text);
      var msg = "From : "+Name+ "\n----------\n```\n" + data.files[0].preview + "\n```\n----------\n" + text;
    }
    else
    {
      var msg = "From : "+Name+ "\n----------\n```\n" + data.files[0].preview + "\n```\n----------\n" + data.text;
    }
    return msg;
  }
  //スニペットに付属テキストは存在せず、スニペットも空でないとき
  else if(data.files[0].preview != "")
  {
    var msg = "From : "+Name+ "\n----------\n```\n" + data.files[0].preview + "\n```";
    return msg;
  }
  //付属テキストもスニペットも空の場合
  else
  {
    console.error('テキストを解析できません。プログラムを終了します。');
  }
}
