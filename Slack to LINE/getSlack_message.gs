function getSlack_message(event)
{
  //Slackにテキスト以外のメッセージが送られた場合
  if (event.subtype != undefined)
  {
    switch(event.subtype)
    {
      //Botからのテキスト発言
      case 'bot_message':
        console.warn('Warning:この投稿はBotから送信されたものです。プログラムを終了します。');
        break;

      //Slackにファイルが共有された場合
      case 'file_share':

        //LINEからSlackに転送するBotのBot_IDをスクリプトのプロパティから呼び出し
        var slack_bot_ID = PropertiesService.getScriptProperties().getProperty('slack_bot_ID');

        //画像の投稿者が転送Botなら
        if (event.user == slack_bot_ID)
        {
          console.warn('Warning:この投稿はLINEから転送されたものです。プログラムを終了します。');
        }
        //pngかjpgの画像ファイルなら
        else if((event.files[0].filetype == 'png') || (event.files[0].filetype == 'jpg') || (event.files[0].filetype == 'jpeg'))
        {
          console.info('この投稿は画像ファイルです。');
          ｓ
          postLINE_image(event.files[0]);

          //画像にメッセージが付属されている場合
          if((event.text != "") || (text!=undefined))
          {
            console.info('この画像データにはテキストデータが含まれています。テキスト用の処理へ移行します。');

            //送信する文章を作成
            var msg = make_message(event.text, event.user);

            console.info('LINE用に生成したメッセージを出力します。');
            console.log(msg);

            return msg;
          }
          else
          {
            console.info('Success:転送完了。プログラムを終了します。');
          }
        }
        //Slackにスニペットが送られた場合
        else if(event.files[0].filetype == 'text')
        {
          console.info('このデータはスニペットです。');

          //スニペット用の送信する文章を作成
          var msg = make_snipetMessage(event, event.user);

          console.info('LINE用に生成したメッセージを出力します。');
          console.log(msg);

          return msg;
        }
        //その他のファイルが送られた場合
        else
        {
          console.info('このデータはファイルデータです。');

          var url = getSlack_filesUrl(event.files[0]);

          return url;
        }
        break;

      //Slackでメッセージが削除された場合
      case 'message_deleted':
        console.warn('このデータは投稿の削除です。指定の投稿の削除には対応していないためプログラムを終了します。');
        break;

      default:
        console.error('不明なデータです。プログラムを終了します。');
        break;
    }
  }
  //Slackにテキストメッセージが送信された場合
  else
  {
    console.info('このデータはテキストメッセージです。テキスト用の処理へ移行します。');

    if((event.text != "") || (text!=undefined))
    {
      //送信する文章を作成
      var msg = make_message(event.text, event.user);
    }
    console.info('LINE用に生成したメッセージを出力します。');
    console.log(msg);

    return msg;
  }
}
