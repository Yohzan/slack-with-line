function emoji(text)
{
  var msg;

  if(text.match(/:heart_eyes:/))
  {
    msg = text.replace(':heart_eyes:','\uDBC0\uDC078');
  }
  else if(text.match(/:smile:/))
  {
    msg = text.replace(':smile:','\uDBC0\uDC79');
  }
  else if(text.match(/:flushed:/))
  {
    msg = text.replace(':flushed:','\uDBC0\uDC7A');
  }
  else if(text.match(/:cold_sweat:/))
  {
    msg = text.replace(':cold_sweat:','\uDBC0\uDC7B');
  }
  else if(text.match(/:sob:/))
  {
    msg = text.replace(':sob:','\uDBC0\uDC7C');
  }
  else if(text.match(/:scream:/))
  {
    msg = text.replace(':scream:','\uDBC0\uDC7D');
  }
  else if(text.match(/:rage:/))
  {
    msg = text.replace(':rage:','\uDBC0\uDC7E');
  }
  else if(text.match(/:stuck_out_tongue_winking_eyes:/))
  {
    msg = text.replace(':stuck_out_tongue_winking_eyes:','\uDBC0\uDC8C');
  }
  else if(text.match(/:blush:/))
  {
    msg = text.replace(':blush:','\uDBC0\uDC8D');
  }
  else if(text.match(/:stuck_out_tongue_closed_eyes:/))
  {
    msg = text.replace(':stuck_out_tongue_closed_eyes:','\uDBC0\uDC8E');
  }
  else if(text.match(/:wink:/))
  {
    msg = text.replace(':wink:','\uDBC0\uDC8F');
  }
  else
  {
    msg = text;
  }

  return msg;
}
