function postLINE_image(img)
{
  //Slackのトークンをプロパティから呼び出し
  var slack_token = PropertiesService.getScriptProperties().getProperty('slack_token');

  //LINE→Slack転送時に記録したグループIDをスプレッドシートから呼び出し
  var sheet_ID = PropertiesService.getScriptProperties().getProperty('sheet_ID');
  var sheet_name = PropertiesService.getScriptProperties().getProperty('sheet_name');
  var spreadsheet = SpreadsheetApp.openById(sheet_ID);
  var sheet = spreadsheet.getSheetByName(sheet_name);
  var ID = sheet.getRange("A1").getValue();　

  console.log('送信先LINEグループIDは"' + ID + '"です。');

  //post送信するデータ
  var data={
    token:slack_token,
    file:img.id
   };
  var slack_option={
    'method':'POST',
    'payload':data
  };
  //投稿された画像をSlackAPIからパブリックにする。
  UrlFetchApp.fetch('https://slack.com/api/files.sharedPublicURL',slack_option);

  //画像ファイルが直接Slackに投稿されている場合
  if(img.permalink_public!=undefined)
  {
    console.info('画像はSlackに直接投稿されています。処理を続行します。');

    //投稿された画像URLをWebページのコードから抽出
    var http = UrlFetchApp.fetch(img.permalink_public).getContentText();

    console.info('画像URL抽出のために獲得したHTMLコードを出力します。');
    console.log(http);

    //"<img src="から始まって"">"で終わる行を抽出し、不要なところを削除しURLだけ抽出する。
    var renge = /<img src=.*?">/;
    var preUrl = String(http.match(renge));
    var trueUrl = preUrl.replace('<img src="', '').replace('">', '');
    console.log('画像URLは"' + trueUrl + '"です。');

    //cloudinaryでアップロードするためにプリセットを設定し、そのIDをプロパティから呼び出す。
    var cloudinary_preset_ID = PropertiesService.getScriptProperties().getProperty('cloudinary_preset_ID');

    //post送信するデータ
    var formData = {
      'file': trueUrl,
      'upload_preset': cloudinary_preset_ID
    };
    var options = {
      'method' : 'post',
      'payload' : formData
    };

    //cloudinaryに画像URLを送信して、加工画像を獲得する。
    var cloudinary = UrlFetchApp.fetch('https://api.cloudinary.com/v1_1/dy5hkichi/image/upload/', options);

    //レスポンスを使える形に変形
    var image = JSON.parse(cloudinary.getContentText());

    console.info('画像を加工するためにCloudinaryに画像をアップロードしました。レスポンスを出力します。');
    console.log(image);

    //もし画像の幅が高さよりも長いなら
    if(image.width >= image.height)
    {
      //オリジナル画像 サイズと形式に制限があるためJPEGファイル、幅を800pxに変更
      var originUrl = 'https://res.cloudinary.com/dy5hkichi/image/upload/c_pad,w_800/v' + image.version +'/' + image.public_id + '.jpg';
      //サムネ用画像 サイズと形式に制限があるためJPEGファイル、幅を200pxに変更
      var previewUrl = 'https://res.cloudinary.com/dy5hkichi/image/upload/c_pad,w_200/v' + image.version +'/' + image.public_id + '.jpg';
    }
    //もし画像の高さが幅よりも長いなら
    else
    {
      //オリジナル画像 サイズと形式に制限があるためJPEGファイル、高さを800pxに変更
      var originUrl = 'https://res.cloudinary.com/dy5hkichi/image/upload/c_pad,h_800/v' + image.version +'/' + image.public_id + '.jpg';
      //サムネ用画像 サイズと形式に制限があるためJPEGファイル、高さを200pxに変更
      var previewUrl = 'https://res.cloudinary.com/dy5hkichi/image/upload/c_pad,h_200/v' + image.version +'/' + image.public_id + '.jpg';
    }


    var postData = {
      "to": ID,
      "messages": [{
        "type": "image",
        "originalContentUrl": originUrl,
        "previewImageUrl": previewUrl
      }]
    };

    var line_token = PropertiesService.getScriptProperties().getProperty('line_token');

    var headers = {
      "Content-Type": "application/json; charset=UTF-8",
      'Authorization': 'Bearer ' + line_token,
    };
    var options = {
      "method": "post",
      "headers": headers,
      "payload": JSON.stringify(postData)
    };
    return UrlFetchApp.fetch('https://api.line.me/v2/bot/message/push', options);
  }
  //画像ファイルがクラウド経由で共有されている場合
  else
  {
    console.info('画像はクラウド経由で共有されました。クラウド上の画像のURLを取得し転送します。');
    console.log('クラウド上の画像のURLは"' + img.url_private + '"です。転送処理に移行します。');
    return postLINE_Message(img.url_private);
  }
}
