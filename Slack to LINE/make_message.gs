function make_message(text, userId)
{
  //SlackAppの導入
  var slack_token = PropertiesService.getScriptProperties().getProperty('slack_token');
  var slackApp = SlackApp.create(slack_token);

  //ユーザーIDから送信者のユーザーの表示名を取得します。
  var userInfo = slackApp.usersInfo(userId);
  console.info('ユーザ名取得のため、ユーザ情報を獲得しました。出力します。');
  console.log(userInfo);

  var Name = userInfo.user.profile.display_name;

  //Slackからの投稿に絵文字が含まれている場合
  if(text.match(/:/))
  {
    console.info('このテキストメッセージには絵文字が含まれています。LINE用に変換します。');
    //LINEでの絵文字に変換
    text = emoji(text);
  }

  var msg = "From : "+Name+ "\n----------\n" +text;　　//送信する本文
  return msg;
}